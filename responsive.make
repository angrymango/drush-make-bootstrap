; Bootstrap .make file for the profile -- run this if don't have an existing
; core installation to drop the profile into.

core = 7.x

api = 2
 
projects[drupal][version] = 7.22
projects[drupal][patch][] = "http://drupal.org/files/1356276-base_profiles_variable-42.patch"

projects[responsive][type] = "profile"
projects[responsive][download][type] = "git"
projects[responsive][download][url] = "git@bitbucket.org:angrymango/responsive-installation-profile.git"
projects[responsive][download][revision] = "master"