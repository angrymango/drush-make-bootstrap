; Bootstrap .make file for the profile -- run this if don't have an existing
; core installation to drop the profile into.

core = 7.x

api = 2
 
projects[drupal][version] = 7.22
projects[drupal][patch][] = "http://drupal.org/files/1356276-base_profiles_variable-42.patch"

; Run the makefile from the drush.make file in this profile.
projects[dma][type] = "profile"
projects[dma][download][type] = "git"
projects[dma][download][url] = "git@bitbucket.org:angrymango/dma-installation-profile.git"
projects[dma][download][revision] = "master"