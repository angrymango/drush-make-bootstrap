; Bootstrap .make file for the profile -- run this if don't have an existing
; core installation to drop the profile into.

core = 7.x

api = 2
 
projects[drupal][version] = 7.22
projects[drupal][patch][] = "http://drupal.org/files/1356276-base_profiles_variable-42.patch"

; Run the makefile from the drush.make file in this profile.
projects[angrymango][type] = "profile"
projects[angrymango][download][type] = "git"
projects[angrymango][download][url] = "git@bitbucket.org:angrymango/angrymango-installation-profile.git"
projects[angrymango][download][revision] = "master"