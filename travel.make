; Bootstrap .make file for the profile -- run this if don't have an existing
; core installation to drop the profile into.

core = 7.x

api = 2
 
projects[drupal][version] = 7.22
projects[drupal][patch][] = "http://drupal.org/files/1356276-base_profiles_variable-42.patch"

projects[travel][type] = "profile"
projects[travel][download][type] = "git"
projects[travel][download][url] = "git@bitbucket.org:angrymango/travel-installation-profile.git"
projects[travel][download][revision] = "master"